from django.shortcuts import render
from rest_framework.generics import ListAPIView, RetrieveAPIView

from core.models import Products
from core.serializers import ProductSerializer, ProductDetailSerializer


def product_detail(request, pk):
    item = Products.objects.get(pk=pk)
    return render(
        request,
        'product_detail.html',
        {'product': item}
    )


def product_list(request):
    items = Products.objects.all()
    return render(
        request,
        'main.html',
        {
            'products': items
        }
    )


class ProductListView(ListAPIView):
    queryset = Products.objects.all()
    serializer_class = ProductSerializer


class ProductDetailView(RetrieveAPIView):
    queryset = Products.objects.all()
    serializer_class = ProductDetailSerializer
    lookup_field = 'pk'
